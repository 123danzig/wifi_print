Wi-Fi connectivity manipulation:
1) Showing a list of all Wi-Fi access points
2) Highlighting the best possible connecting point (where RSSI is the closest to zero)
3) Prompting the user for the password to that access point
4) Connect to that access point
5) Showing that you were able to access the Internet though that access point